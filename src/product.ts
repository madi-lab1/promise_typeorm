import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"


AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
   

    const products = await productRepository.find()
    console.log("Loaded products: ", products)


    const updatedProduct = await productRepository.findOneBy({id:1 })
    console.log(updatedProduct)
    updatedProduct.price=80
    await productRepository.save(updatedProduct)
}).catch(error => console.log(error))
