import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const userRepository = AppDataSource.getRepository(User)
    const user = new User()
    user.login = "user1"
    user.name = "user 1"
    user.password = "Pass@1234"
    await AppDataSource.manager.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await userRepository.find()
    console.log("Loaded users: ", users)



}).catch(error => console.log(error))
